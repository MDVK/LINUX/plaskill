#!/bin/bash
## Bash Script to fix bug memory leak on plasmashell
## By Aditya Pratama	| 2018
## https://gitlab.com/MDVK/LINUX/plaskill
LINE="-------------------------------------------------------"

if [ "$(whoami)" != "root" ]
then
  echo "You have to run this script as Superuser! (sudo)"
  exit 1
fi 

## Copy File
 cp plaskill plaskill_exec /usr/bin/
 cd /usr/bin/

 ## change mod
 chmod +x plaskill

## Add to Startup
 ln -s -f /usr/bin/plaskill /home/$(hostname)/.config/autostart-scripts


###############################################

echo -e $LINE
echo -e "This script  successfully installed "
echo -e "and will running for next Startup"
echo -e $LINE
